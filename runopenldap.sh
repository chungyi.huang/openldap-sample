#!/bin/bash -e
#docker run -p 389:389 -p 636:636 --name my-openldap \
#          --hostname ldap.my.com --detach  \
#         --volume /home/yungho/ssl:/container/service/slapd/assets/certs \
#	--env LDAP_TLS_CRT_FILENAME=ldap.pem \
#	--env LDAP_TLS_KEY_FILENAME=ldap-key.pem \
#	--env LDAP_TLS_CA_CRT_FILENAME=ca.pem \
#        --env LDAP_ORGANISATION="My Company" \
#	--env LDAP_DOMAIN="my.com" \
#	--env LDAP_ADMIN_PASSWORD="admin" \
#          osixia/openldap:1.5.0

#docker run -p 443:443 \
#          --name my-phpldapadmin \
#          --link my-openldap:ldap-host \
#          --env PHPLDAPADMIN_LDAP_HOSTS=ldap-host \
#          --detach osixia/phpldapadmin:0.9.0

#docker run --name ldap-service --hostname ldap-service --detach osixia/openldap:1.5.0
docker run -d -p 389:389 -p 636:636 --name ldap-service --hostname ldap-service --detach \
         --volume $PWD/ssl:/container/service/slapd/assets/certs \
	--env LDAP_TLS_CRT_FILENAME=ldap.pem \
	--env LDAP_TLS_KEY_FILENAME=ldap-key.pem \
	--env LDAP_TLS_CA_CRT_FILENAME=ca.pem \
        --env LDAP_TLS_VERIFY_CLIENT=try \
        --env LDAP_ORGANISATION="My.com" \
	--env LDAP_DOMAIN="my.com" \
	--env LDAP_ADMIN_PASSWORD="admin" \
        osixia/openldap:1.5.0-pp
docker run -d -p 6443:443 --name phpldapadmin-service --hostname phpldapadmin-service --link ldap-service:ldap-host --env PHPLDAPADMIN_LDAP_HOSTS=ldap-host --detach osixia/phpldapadmin:0.9.0

PHPLDAP_IP=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" phpldapadmin-service)

echo "Go to: https://$PHPLDAP_IP"
echo "Login DN: cn=admin,dc=my,dc=com"
echo "Password: admin"
